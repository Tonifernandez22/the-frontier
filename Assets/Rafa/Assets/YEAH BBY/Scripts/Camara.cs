﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camara : MonoBehaviour
{
    bool miraIzq;
    public GameObject Rango;
    float reloj;
    bool relojsuma;
    bool relojresta;
    // Start is called before the first frame update
    void Start()
    {
        miraIzq = true;
        relojsuma = true;
        relojresta = false;
        reloj = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (reloj >= 200f)
        {
            relojsuma=false;
            relojresta = true;
        }
        if (reloj <= 0f)
        {
            relojresta=false;
            relojsuma = true;
        }

        if (relojsuma == true)
        {
            reloj++;
            if (miraIzq == true)
            {
                transform.Rotate(0, 180, 0);
                miraIzq = false;
            }
        }
        if (relojresta == true)
        {
            reloj--;
            if (miraIzq == false)
            {
                transform.Rotate(0, 180, 0);
                miraIzq = true;
            }
        }
    }
}
