﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Personaje : MonoBehaviour
{
    public float Speed;
    bool andar = false;
    bool miraDer;
    public bool stop;
    public bool detectar = false;
    public bool EstoyEscondido;
    public bool EstoyAgachado;
    public static float PosicionPersonajeX;
    public static bool MeHanPillado;
    public static bool DetenCocheChico;
    // Start is called before the first frame update
    void Start()
    {
        miraDer = true;
        stop = false;
        EstoyEscondido = false;
        DetenCocheChico = false;
        MeHanPillado = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (stop == true)
        {
            transform.Translate(Speed * Time.deltaTime * 0, 0, 0);
        }
        PosicionPersonajeX = transform.position.x;

        //Andar
        if (Input.GetKey(KeyCode.RightArrow) && stop == false)
        {
            transform.Translate(Speed * Time.deltaTime * 1, 0, 0);
            gameObject.GetComponent<Animator>().SetBool("Andar", true);
            andar = true;
            if (miraDer == false)
            {
                transform.Rotate(0, 180, 0);
                miraDer = true;
            }
        }
        if (Input.GetKey(KeyCode.LeftArrow) && stop == false)
        {
            transform.Translate(Speed * Time.deltaTime * 1, 0, 0);
            gameObject.GetComponent<Animator>().SetBool("Andar", true);
            andar = true;
            if (miraDer == true)
            {
                transform.Rotate(0, 180, 0);
                miraDer = false;
            }
        }

        //Dejar de andar
        if (Input.GetKeyUp(KeyCode.RightArrow) && stop == false)
        {
            transform.Translate(Speed * Time.deltaTime * 1, 0, 0);
            gameObject.GetComponent<Animator>().SetBool("Andar", false);
            andar = false;
        }
        if (Input.GetKeyUp(KeyCode.LeftArrow) && stop == false)
        {
            transform.Translate(Speed * Time.deltaTime * 1, 0, 0);
            gameObject.GetComponent<Animator>().SetBool("Andar", false);
            andar = false;
        }

        //Correr
        if (Input.GetKey(KeyCode.LeftShift) && andar == true)
        {
            transform.Translate(Speed * Time.deltaTime * 2, 0, 0);
            gameObject.GetComponent<Animator>().SetBool("Correr", true);
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            transform.Translate(Speed * Time.deltaTime * 2, 0, 0);
            gameObject.GetComponent<Animator>().SetBool("Correr", false);
        }

        //Agacharse
        if (Input.GetKey(KeyCode.C) && MeHanPillado==false)
        {
            gameObject.GetComponent<Animator>().SetBool("Agacharse", true);
            gameObject.GetComponent<Animator>().SetBool("Levantarse", false);
            gameObject.GetComponent<Animator>().SetBool("Andar", false);
            gameObject.GetComponent<Animator>().SetBool("Correr", false);
            stop = true;
            EstoyAgachado = true;
        }
        if (Input.GetKeyUp(KeyCode.C))
        {
            gameObject.GetComponent<Animator>().SetBool("Levantarse", true);
            gameObject.GetComponent<Animator>().SetBool("Agacharse", false);
            stop = false;
            EstoyAgachado = false;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag=="Luz" && EstoyEscondido==false)
        {
            MeHanPillado = true;
            gameObject.GetComponent<Animator>().SetBool("Rendirse", true);
            stop = true;
            DetenCocheChico = true;
           
        }
        
    }

    private void OnTriggerStay2D(Collider2D collision)
    {if (collision.gameObject.tag == "Escondites" && EstoyAgachado==true )
        {
            EstoyEscondido = true;

        }
       
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Escondites")
        {
            EstoyEscondido = false;
        }


    }
}
