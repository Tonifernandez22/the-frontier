﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coche : MonoBehaviour
{
     float velocidad;
    public GameObject Rueda1;
    public GameObject Rueda2;
     float VelocidadRuedas;
    // Start is called before the first frame update
    void Start()
    {
        velocidad =6.5f;
        VelocidadRuedas =5;
    }

    // Update is called once per frame
    void Update()
    {
        if (MovimientoPersonaje.DetenCoche == false && CargaPersonaje.PersonajeElegido == 1)
        {
            
                float vX = velocidad * Time.deltaTime;
                transform.Translate(-vX, 0, 0);

                Rueda1.transform.Rotate(0, 0, VelocidadRuedas);
                Rueda2.transform.Rotate(0, 0, VelocidadRuedas);
            
        }
        if (Personaje.DetenCocheChico == false && CargaPersonaje.PersonajeElegido == 2)
        {
            float vX = velocidad * Time.deltaTime;
            transform.Translate(-vX, 0, 0);

            Rueda1.transform.Rotate(0, 0, VelocidadRuedas);
            Rueda2.transform.Rotate(0, 0, VelocidadRuedas);
        }
        if (transform.position.x < -30f)
        {
            Destroy(gameObject);
        }

    }
    

}
