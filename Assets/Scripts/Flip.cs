﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Flip : MonoBehaviour
{
    public Animator animator;
    private SpriteRenderer OpacidadNinja;
    public static bool EstoySentado;
    
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        OpacidadNinja = GetComponent<SpriteRenderer>();
        EstoySentado = false;
    }

    // Update is called once per frame
    void Update()
    {if(MovimientoPersonaje.Velocidad != 0)
        {
            
            animator.SetBool("Caminando", true);
         if (Input.GetKey(KeyCode.LeftShift))
            {
                animator.SetBool("Shift_apretado", true);
                MovimientoPersonaje.Speed = 5;
            }else if(Input.GetKeyUp(KeyCode.LeftShift))
            {
                animator.SetBool("Shift_apretado", false);
                MovimientoPersonaje.Speed = 3;
            }
        }
        else
        {
            animator.SetBool("Shift_apretado", false);
            MovimientoPersonaje.Speed = 3;

            animator.SetBool("Caminando", false);
        }
        if (Input.GetKey(KeyCode.C) )
        {
            animator.SetBool("Agacharse", true);
            EstoySentado = true;
            animator.SetBool("Levantarse", false);


        }
        if (Input.GetKeyUp(KeyCode.C))
        {
            
            
                animator.SetBool("Levantarse", true);


                EstoySentado = false;

                animator.SetBool("Agacharse", false);
            

        }
        if (MovimientoPersonaje.DetenCoche == true)
        {
            animator.SetBool("Rendirse", true);
            animator.SetBool("Caminando", false);
            animator.SetBool("Shift_apretado", false);
            animator.SetBool("Agacharse", false);
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                SceneManager.LoadScene("MenuPrincipal");
            }
        }
    }
}
