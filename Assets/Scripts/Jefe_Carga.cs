﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jefe_Carga : MonoBehaviour
{
    public GameObject ProtagonistaChica;
    public GameObject ProtagonistaChico;
    private void Awake()
    {
        if (CargaPersonaje.PersonajeElegido == 1)
        {
            ProtagonistaChica.SetActive(true);
        }
        else if(CargaPersonaje.PersonajeElegido == 2)
        {
            ProtagonistaChico.SetActive(true);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
