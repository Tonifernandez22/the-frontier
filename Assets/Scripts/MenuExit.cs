﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuExit : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnMouseOver()
    {
        ScriptExit.AclararExit = true;
        if (Input.GetMouseButtonDown(0))
        {
            Application.Quit();
        }

    }
    private void OnMouseExit()
    {
        ScriptExit.AclararExit = false;
    }
}
