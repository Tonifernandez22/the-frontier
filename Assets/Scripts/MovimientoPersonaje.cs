﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MovimientoPersonaje : MonoBehaviour
{

    public static float Speed;
    public bool MirandoDerecha;
    public static float Velocidad;
    public static bool Indetectable;
    public static float PosicionPersonajeX;
    public static bool DetenCoche;
    
    // Start is called before the first frame update
    void Start()
    {
        MirandoDerecha = true;
        Speed = 3;
        Indetectable = false;
        DetenCoche = false;
        MirandoDerecha = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (DetenCoche == false && Flip.EstoySentado==false)
        {
            Velocidad = Input.GetAxisRaw("Horizontal");
            if (Velocidad > 0)
            {
                transform.Translate(Speed * Time.deltaTime * 1, 0, 0);

                if (MirandoDerecha == false)
                {
                    transform.Rotate(0, 180, 0);
                    MirandoDerecha = true;

                }
            }
            else if (Velocidad < 0)
            {
                transform.Translate(Speed * Time.deltaTime * 1, 0, 0);

                if (MirandoDerecha == true)
                {
                    transform.Rotate(0, 180, 0);
                    MirandoDerecha = false;
                }
            }
            PosicionPersonajeX = transform.position.x;
            
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Escondites" && Flip.EstoySentado==true)
        {
               
            Indetectable = true;
        }
        
    }
    
    private void OnTriggerEnter2D(Collider2D collision)
    {if(collision.gameObject.tag=="Luz" && Indetectable==false)
        {
            
            DetenCoche = true;
            
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {if(collision.gameObject.tag == "Escondites")

            Indetectable = false;
    }
}

