﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptChica : MonoBehaviour
{
    public SpriteRenderer spriterendered;
    public static bool AclararChica;
    // Start is called before the first frame update
    void Start()
    {
        spriterendered = GetComponent<SpriteRenderer>();
        spriterendered.color = new Color(spriterendered.color.r, spriterendered.color.g, spriterendered.color.b, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if(AclararChica == true)
        {
            spriterendered.color = new Color(spriterendered.color.r, spriterendered.color.g, spriterendered.color.b, 1);
        }else if (AclararChica == false)
        {
            spriterendered.color = new Color(spriterendered.color.r, spriterendered.color.g, spriterendered.color.b, 0);
        }
    }
}
