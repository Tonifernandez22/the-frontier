﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptChico : MonoBehaviour
{
    public SpriteRenderer spriterendered;
    public static bool AclararChico;
    // Start is called before the first frame update
    void Start()
    {
        spriterendered = GetComponent<SpriteRenderer>();
        spriterendered.color = new Color(spriterendered.color.r, spriterendered.color.g, spriterendered.color.b, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if(AclararChico == true)
        {
            spriterendered.color = new Color(spriterendered.color.r, spriterendered.color.g, spriterendered.color.b, 1);
        }else if (AclararChico == false)
        {
            spriterendered.color = new Color(spriterendered.color.r, spriterendered.color.g, spriterendered.color.b, 0);
        }
    }
}
