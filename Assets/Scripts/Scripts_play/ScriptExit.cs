﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptExit : MonoBehaviour
{
    public static bool AclararExit;
    public SpriteRenderer spriterendered;
    // Start is called before the first frame update
    void Start()
    {
        spriterendered = GetComponent<SpriteRenderer>();
        spriterendered.color = new Color(spriterendered.color.r, spriterendered.color.g, spriterendered.color.b, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (AclararExit == true)
        {
            spriterendered.color = new Color(spriterendered.color.r, spriterendered.color.g, spriterendered.color.b, 1);
        }
        else if (AclararExit == false)
        {
            spriterendered.color = new Color(spriterendered.color.r, spriterendered.color.g, spriterendered.color.b, 0);
        }
    }
}
