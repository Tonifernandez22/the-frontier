﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptPlay : MonoBehaviour
{
    public SpriteRenderer spriterendered;
    public static bool Aclarar;
    // Start is called before the first frame update
    void Start()
    {
        Aclarar = false;
        spriterendered = GetComponent<SpriteRenderer>();
        spriterendered.color = new Color(spriterendered.color.r, spriterendered.color.g, spriterendered.color.b, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (Aclarar == true)
        {
            spriterendered.color = new Color(spriterendered.color.r,spriterendered.color.g,spriterendered.color.b,1);
        }else if (Aclarar == false)
        {
            spriterendered.color = new Color(spriterendered.color.r, spriterendered.color.g, spriterendered.color.b, 0);
        }
    }
}
