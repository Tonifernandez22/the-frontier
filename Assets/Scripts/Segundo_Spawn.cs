﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Segundo_Spawn : MonoBehaviour
{
    private float TiempoDisparo;
     float CadenciaCoches;
    public GameObject Coche;
    public static bool ActivarSpawn;

   
    // Start is called before the first frame update
    void Start()
    {
               CadenciaCoches = 5.5f;
               ActivarSpawn = false;
           TiempoDisparo = Time.time;

    }

    // Update is called once per frame
    void Update()
    {
        float TDif = Time.time - TiempoDisparo;
        if (ActivarSpawn == true)
        {
            
            

            if (TDif >= CadenciaCoches)
        {
            GameObject clone = Instantiate(Coche, transform.position, Quaternion.identity) as GameObject;
            clone.SetActive(true);

            TiempoDisparo = Time.time;
        }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Destroy(this);
       
    }
}

