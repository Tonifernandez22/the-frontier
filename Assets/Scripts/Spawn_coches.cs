﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn_coches : MonoBehaviour
{
    private float TiempoDisparo;
     float CadenciaCoches;
    public GameObject Coche;
    public GameObject Coche_SegundoSpawn;
    
    // Start is called before the first frame update
    void Start()
    {
        TiempoDisparo = Time.time;
        GameObject clone = Instantiate(Coche, transform.position, Quaternion.identity) as GameObject;
        clone.SetActive(true);
        CadenciaCoches = 6.2f;
    }

    // Update is called once per frame
    void Update()
    {
        float TDif = Time.time - TiempoDisparo;
        if (TDif >= CadenciaCoches)
        {
            GameObject clone = Instantiate(Coche, transform.position, Quaternion.identity) as GameObject;
            clone.SetActive(true);

            TiempoDisparo = Time.time;

        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Coche_SegundoSpawn.SetActive(true);
        Destroy(this);
        Segundo_Spawn.ActivarSpawn = true;
    }
}
